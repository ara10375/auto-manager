#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np


# In[2]:


from pathlib import Path


# In[3]:


from datetime import datetime


# # Merge the three DRB outputs to a single dataframe and then export as a new csv

# In[4]:


now = datetime.now()


# In[5]:


print(Path.cwd())


# In[6]:


input_folder = Path.cwd() / "MPSR-Input" / now.strftime("%Y-%m-%d")


# In[7]:


input_folder


# In[8]:


drb1 = input_folder / "DRB1.csv"
drb2 = input_folder / "DRB2.csv"
drb3 = input_folder / "DRB3.csv"


# In[9]:


df1 = pd.read_csv(drb1)


# In[10]:


df2 = pd.read_csv(drb2)


# In[11]:


df3 = pd.read_csv(drb3)


# In[12]:


df1.head()


# # Setup Output 

# In[13]:


output_dir = Path('/Users/acrager/Python/MPSR/' + now.strftime("%Y-%m-%d"))


# In[14]:


output_dir.mkdir(exist_ok=True)


# In[15]:


output_file0 = output_dir / f'DRB_Report_{now.strftime("%Y%m%d_%H%M%S")}.csv'


# In[16]:


output_file1 = output_dir / f'DRB_Analysis_{now.strftime("%Y%m%d_%H%M%S")}.xlsx'


# In[17]:


output_file2 = output_dir / f'MPSR_Tables_{now.strftime("%Y%m%d_%H%M%S")}.xlsx'


# ## https://datacarpentry.org/python-ecology-lesson/05-merging-data/

# In[18]:


vertical_stack = pd.concat([df1, df2, df3], axis=0, sort=False)


# In[19]:


vertical_stack.rename(columns={"Resolution":"JiraResolution"}, inplace=True)


# In[20]:


vertical_stack['Resolution'] = np.where(vertical_stack['Status']== 'Resolved', "Resolved", "Unresolved")


# In[21]:


vertical_stack.rename(columns={"Custom field (Ground or Mission Ticket ID)":"Ground or Mission Ticket ID"}, inplace=True)


# # Update data

# In[22]:


vertical_stack['Fix Version/s'] = vertical_stack['Fix Version/s'].fillna("Not Assigned")


# In[23]:


vertical_stack['Ground or Mission Ticket ID'] = vertical_stack['Ground or Mission Ticket ID'].fillna("N/A")


# # Write DataFrame to CSV

# In[24]:


vertical_stack.to_csv(output_file0, index=False)


# # Generate a Pivot Table: Resolved vs Unresolved for build #

# ## https://pbpython.com/pandas-pivot-table-explained.html

# In[25]:


vertical_stack.head(2)


# In[26]:


vertical_stack[vertical_stack.Status != "Rejected"]


# In[27]:


# Filter out those items in filter_list
filter_list = ["Rejected", "Dispositioning"]
filtered_df = vertical_stack[~vertical_stack['Status'].isin(filter_list)]


# In[28]:


filtered_df.info()


# In[29]:


# # Filter out those items in filter_list
# filter_list = ["Rejected"]
# filtered_df = vertical_stack[~vertical_stack['Resolution'].isin(filter_list)]


# In[30]:


pvt = pd.pivot_table(filtered_df,
                     index=["Status"], 
                     columns= ["Priority"], 
                     values = "Issue key", 
                     aggfunc = {"Issue key": len}, fill_value = 0,
                     margins=True)


# In[31]:


pvt


# In[32]:


pvt2 = pd.pivot_table(filtered_df,
                     index=["Resolution"], 
                     columns= ["Priority"], 
                     values = "Issue key", 
                     aggfunc = {"Issue key": len}, fill_value = 0,
                     margins=True)
pvt2


# In[33]:


resolution_summary = pd.pivot_table(filtered_df,
                     index=["Resolution", "Status"], 
                     columns= ["Priority"], 
                     values = "Issue key", 
                     aggfunc = {"Issue key": len}, fill_value = 0,
                     margins=True)
resolution_summary


# In[34]:


major_remaining = filtered_df[
                       (filtered_df["Resolution"] == "Unresolved") &
                       (filtered_df["Priority"] == "Major")
                      ]
major_remaining = major_remaining[['Issue key', "Ground or Mission Ticket ID", "Summary", "Fix Version/s", "Status", "Component/s"]].sort_values(by="Status")


# In[35]:


crit_remaining = filtered_df[
                       (filtered_df["Resolution"] == "Unresolved") &
                       (filtered_df["Priority"] == "Critical")
                      ]
crit_remaining = crit_remaining[['Issue key', "Ground or Mission Ticket ID", "Summary", "Fix Version/s", "Status", "Component/s"]].sort_values(by="Status")


# In[36]:


minor_remaining = filtered_df[
                       (filtered_df["Resolution"] == "Unresolved") &
                       (filtered_df["Priority"] == "Minor")
                      ]
minor_remaining = minor_remaining[['Issue key', "Ground or Mission Ticket ID", "Summary", "Fix Version/s", "Status", "Component/s"]].sort_values(by="Status")


# In[37]:


pvt_dur_bld_aff = pd.pivot_table(filtered_df,
                     index=["Resolution", "Affects Version/s"], 
                     columns= ["Priority"], 
                     values = "Issue key", 
                     aggfunc = {"Issue key": len}, fill_value = 0,
                     margins=True)


# In[38]:


pvt_dur_bld_res = pd.pivot_table(filtered_df,
                     index=["Resolution", "Fix Version/s"], 
                     columns= ["Priority"], 
                     values = "Issue key", 
                     aggfunc = {"Issue key": len}, fill_value = 0,
                     margins=True)


# In[39]:


pvt_dur_subsys = pd.pivot_table(filtered_df,
                     index=["Resolution", "Component/s"], 
                     columns= ["Priority"], 
                     values = "Issue key", 
                     aggfunc = {"Issue key": len}, fill_value = 0,
                     margins=True)


# # https://xlsxwriter.readthedocs.io/example_pandas_column_formats.html
# # https://xlsxwriter.readthedocs.io/working_with_pandas.html

# # https://pbpython.com/advanced-excel-workbooks.html

# In[40]:


def summary_sheet(writer, dataframe):

    dataframe.to_excel(writer, 
                       sheet_name='Summary', 
                       engine='xlsxwriter')
    sheet = writer.sheets['Summary']

    # Set the column width and format.
    sheet.set_column('A:A', 
                         10
                        )
    sheet.set_column('B:B', 
                         18, 
                         writer.book.add_format({'bold': False})
                        )
    sheet.set_column('C:C', 
                         10, 
                         writer.book.add_format({'align': 'center'})
                        )
    sheet.set_column('D:D', 
                         10, 
                         writer.book.add_format({'align': 'center'})
                        )
    sheet.set_column('E:E', 
                         10, 
                         writer.book.add_format({'align': 'center'})
                        )
    sheet.set_column('F:F', 
                         10, 
                         writer.book.add_format({'align': 'center', 'bold':True})
                        )
    return sheet


# In[41]:


def remaining_priority(writer, dataframe, priority):
    dataframe.to_excel(writer, 
                       sheet_name=f'Remaining {priority.capitalize()} DURs', 
                       engine='xlsxwriter', 
                       index=False)
    sheet = writer.sheets[f'Remaining {priority.capitalize()} DURs']

    # Set the column width and format.
    sheet.set_column('A:A', 
                         13
                        )
    sheet.set_column('B:B', 
                         15, 
                         writer.book.add_format({'bold': False, 'text_wrap': True})
                        )
    sheet.set_column('C:C', 
                         30, 
                         writer.book.add_format({'align': 'center', 'text_wrap': True})
                        )
    sheet.set_column('D:D', 
                         13, 
                         writer.book.add_format({'align': 'center'})
                        )
    sheet.set_column('E:E', 
                         16, 
                         writer.book.add_format({'align': 'center'})
                        )
    sheet.set_column('F:F', 
                         15, 
                         writer.book.add_format({'align': 'center'})
                        )
    if len(dataframe):
        sheet.add_table(f'A1:F{len(dataframe)+1}', 
                         {'columns': [{'header': item} for item in list(dataframe.columns)],
                          'style': 'Table Style Medium 2'})
    
    return sheet


# In[42]:


def input_data(writer, dataframe):
    dataframe.to_excel(writer, sheet_name='DRB Report', engine='xlsxwriter', index=False)
    sheet = writer.sheets['DRB Report']

    sheet.add_table(f'A1:Z{len(vertical_stack)+1}', 
                        {'columns': [{'header': item} for item in list(vertical_stack.columns)],
                         'style': 'Table Style Medium 2'})
    return sheet


# In[43]:


def pivot_builds(writer, dataframe, sheet_name):

    dataframe.to_excel(writer, 
                       sheet_name=sheet_name, 
                       engine='xlsxwriter')
    sheet = writer.sheets[sheet_name]

    # Set the column width and format.
    sheet.set_column('A:A', 
                         10
                        )
    sheet.set_column('B:B', 
                         18, 
                         writer.book.add_format({'bold': False})
                        )
    sheet.set_column('C:C', 
                         10, 
                         writer.book.add_format({'align': 'center'})
                        )
    sheet.set_column('D:D', 
                         10, 
                         writer.book.add_format({'align': 'center'})
                        )
    sheet.set_column('E:E', 
                         10, 
                         writer.book.add_format({'align': 'center'})
                        )
    sheet.set_column('F:F', 
                         10, 
                         writer.book.add_format({'align': 'center', 'bold':True})
                        )
    return sheet


# In[44]:


output_writer = pd.ExcelWriter(output_file1)

# Design the Input Data sheet
sheet = input_data(output_writer, vertical_stack)

# Design the Resolution Summary Sheet
sheet = summary_sheet(output_writer, resolution_summary)

# Design the Remaining Critical Priority DURs sheet
sheet = remaining_priority(output_writer, crit_remaining, 'critical')

# Design the Remaining Major Priority DURs sheet
sheet = remaining_priority(output_writer, major_remaining, 'major')

# Design the Remaining Minor Priority DURs sheet
sheet = remaining_priority(output_writer, minor_remaining, 'minor')

output_writer.save()


# In[45]:


output_writer = pd.ExcelWriter(output_file2)

# Design the Resolution Summary Sheet
sheet = pivot_builds(output_writer, pvt_dur_bld_aff, "Discr. Status per Build (A)")

# Design the Resolution Summary Sheet
sheet = pivot_builds(output_writer, pvt_dur_bld_res, "Discr. Status per Build (F)")

# Design the Resolution Summary Sheet
sheet = pivot_builds(output_writer, pvt_dur_subsys, "Discr. Status per Subsystem")

# Design the Input Data sheet
sheet = input_data(output_writer, vertical_stack)

output_writer.save()


# In[ ]:





# # messing around with outputting df to png

# In[46]:


# source: https://stackoverflow.com/questions/35634238/how-to-save-a-pandas-dataframe-table-as-a-png


# In[47]:


import pandas as pd
import numpy as np
import dataframe_image as dfi


# In[48]:


png_output = str(output_dir / f"dur_status_per_build_affects_{now.strftime('%Y%m%d_%H%M%S')}.png")
dfi.export(pvt_dur_bld_aff, png_output)

png_output = str(output_dir / f"dur_status_per_build_fix_{now.strftime('%Y%m%d_%H%M%S')}.png")
dfi.export(pvt_dur_bld_res, png_output)

png_output = str(output_dir / f"dur_status_per_subsystem_{now.strftime('%Y%m%d_%H%M%S')}.png")
dfi.export(pvt_dur_subsys, png_output)

png_output = str(output_dir / f"crit_remaining_{now.strftime('%Y%m%d_%H%M%S')}.png")
dfi.export(crit_remaining, png_output)

png_output = str(output_dir / f"major_remaining_{now.strftime('%Y%m%d_%H%M%S')}.png")
dfi.export(major_remaining, png_output)


# # split into multiple dfs and then to images

# In[49]:


filtrr = filtered_df.loc[filtered_df["Resolution"] == "Resolved"]
fltr_df = pd.pivot_table(filtrr,
                     index=["Resolution", "Affects Version/s"], 
                     columns= ["Priority"], 
                     values = "Issue key", 
                     aggfunc = {"Issue key": len}, fill_value = 0,
                     margins=True)
png_output = str(output_dir / f"status_per_build_affects_resolved_{now.strftime('%Y%m%d_%H%M%S')}.png")
dfi.export(fltr_df, png_output)


# In[ ]:


filtrr = filtered_df.loc[filtered_df["Resolution"] == "Unresolved"]
fltr_df = pd.pivot_table(filtrr,
                     index=["Resolution", "Affects Version/s"], 
                     columns= ["Priority"], 
                     values = "Issue key", 
                     aggfunc = {"Issue key": len}, fill_value = 0,
                     margins=True)
png_output = str(output_dir / f"status_per_build_affects_unresolved_{now.strftime('%Y%m%d_%H%M%S')}.png")
dfi.export(fltr_df, png_output)


# In[ ]:


filtrr = filtered_df.loc[filtered_df["Resolution"] == "Resolved"]
fltr_df = pd.pivot_table(filtrr,
                     index=["Resolution", "Fix Version/s"], 
                     columns= ["Priority"], 
                     values = "Issue key", 
                     aggfunc = {"Issue key": len}, fill_value = 0,
                     margins=True)
png_output = str(output_dir / f"status_per_build_fix_resolved_{now.strftime('%Y%m%d_%H%M%S')}.png")
dfi.export(fltr_df, png_output)


# In[ ]:


filtrr = filtered_df.loc[filtered_df["Resolution"] == "Unresolved"]
fltr_df = pd.pivot_table(filtrr,
                     index=["Resolution", "Fix Version/s"], 
                     columns= ["Priority"], 
                     values = "Issue key", 
                     aggfunc = {"Issue key": len}, fill_value = 0,
                     margins=True)
png_output = str(output_dir / f"status_per_build_fix_unresolved_{now.strftime('%Y%m%d_%H%M%S')}.png")
dfi.export(fltr_df, png_output)


# In[ ]:


filtrr = filtered_df.loc[filtered_df["Resolution"] == "Resolved"]
fltr_df = pd.pivot_table(filtrr,
                     index=["Resolution", "Component/s"], 
                     columns= ["Priority"], 
                     values = "Issue key", 
                     aggfunc = {"Issue key": len}, fill_value = 0,
                     margins=True)
png_output = str(output_dir / f"status_per_subsystem_resolved_{now.strftime('%Y%m%d_%H%M%S')}.png")
dfi.export(fltr_df, png_output)


# In[ ]:


filtrr = filtered_df.loc[filtered_df["Resolution"] == "Unresolved"]
fltr_df = pd.pivot_table(filtrr,
                     index=["Resolution", "Component/s"], 
                     columns= ["Priority"], 
                     values = "Issue key", 
                     aggfunc = {"Issue key": len}, fill_value = 0,
                     margins=True)
png_output = str(output_dir / f"status_per_subsystem_unresolved_{now.strftime('%Y%m%d_%H%M%S')}.png")
dfi.export(fltr_df, png_output)


# In[ ]:





# In[ ]:





# In[ ]:




